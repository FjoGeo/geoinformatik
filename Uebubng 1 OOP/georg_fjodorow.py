#Uebung 2 Geoinformatik
#Fjodorow Georg - https://gitlab.com/FjoGeo/geoinformatik

import math


class Punkt:

    punkt_1 = []
    punkt_2 = []
    anzahl = 0

    def __init__(self, *args):
        self.x = args[0]
        self.y = args[1]
        Punkt.anzahl += 1
        if Punkt.anzahl == 1:
            Punkt.punkt_1.append(self.x)
            Punkt.punkt_1. append(self.y)
        if Punkt.anzahl == 2:
            Punkt.punkt_2.append(self.x)
            Punkt.punkt_2.append(self.y)

    def coordinates(self):
        return f"{self.x} {self.y}"

    @staticmethod
    def eu_distance():
        dx = Punkt.punkt_2[0] - Punkt.punkt_1[0]
        dy = Punkt.punkt_2[1] - Punkt.punkt_1[1]
        euklDist = math.sqrt(dx**2 + dy**2)
        return euklDist

    @staticmethod
    def area():
        return 0


class GeographicPoint(Punkt):

    def __init__(self, *args):
        super().__init__(*args)
        self.lat = args[0]
        self.lon = args[1]

    @property
    def lat_lon(self):
        return f"{self.lat} {self.lon}"

    @staticmethod
    def distance_kugeloberflaeche():
        lat1 = Punkt.punkt_2[0]
        lat0 = Punkt.punkt_1[0]
        lon1 = Punkt.punkt_2[1]
        lon0 = Punkt.punkt_1[1]
        la = lat1 - lat0
        lo = lon1 - lon0
        a = math.sin((math.radians(la / 2))) ** 2 + math.cos(math.radians(lat0)) * math.cos(math.radians(lat1)) * \
            math.sin(math.radians(lo / 2)) ** 2
        c = 2 * math.atan2(math.sqrt(a), math.sqrt(1-a))
        d = 6371 * c
        return d


geo_p1 = GeographicPoint(53.551086,9.993682)
geo_p2 = GeographicPoint(55.755826,37.617300)

#print(help(GeographicPoint))
print('Koordinaten (vererbt): '+geo_p1.coordinates())
print(f'Distanz Hamburg - Moskau: {GeographicPoint.distance_kugeloberflaeche()}')


