#UE5 DataMod SS2020 | help: gueren.dinga@hcu-hamburg.de
# Vervollständigt die Funktionen "linearsearch" und "binaeresuche"
# Die Klasse "Point" die ihr für das Erzeugen eurer Punkte (für den Benchmark)
# muss/darf nicht verändert werden.


# imports
from random import randint, random
from timeit import Timer
import numbers, math, timeit, random
import time
from datetime import timedelta
import matplotlib.pyplot as plt
import numbers, math, timeit, random
from matplotlib import style

style.use('ggplot')


# erzeugen einer liste und festlegen des Zielwerts zum Testen
# eurer Funktionen
liste = sorted([randint(0,10) for i in range(20)])
liste = list(range(0, 1000))
suche = 237
n = [5, 10, 25, 50, 75, 100, 250, 500, 1000, 2500, 5000, 10000, 50000, 100000]

def linearesuche(data, value):
    for n, i in enumerate(data):
        if i == value:
            #print(f"gefunden an Position {n} ")
            return n
    return -1


def binaeresuche(data, value):
    l = 0
    r = len(data) - 1

    while l <= r:
        mitte = (r + l) // 2

        if data[mitte] < value:
            l = mitte + 1

        elif data[mitte] > value:
            r = mitte - 1

        else:
            return mitte
    return -1


print(linearesuche(liste,suche))
print(binaeresuche(liste,suche))

# ============================================== Benchmark ============================================================


# Die Klasse müsst ihr nicht anrühren
class Point(object):
    def __init__(self, x, y):
        if isinstance(x, numbers.Number):
            self.x = x
        else:
            raise TypeError("x must be a number")

        if isinstance(y, numbers.Number):
            self.y = y
        else:
            raise TypeError("y must be a number")

    @property
    def x(self):
        return self._x

    @property
    def y(self):
        return self._y

    @x.setter
    def x(self, x):
        if isinstance(x, numbers.Number):
            self._x = x
        else:
            raise TypeError("X muss eine Zahl sein")

    @y.setter
    def y(self, y):
        if isinstance(y, numbers.Number):
            self._y = y
        else:
            raise TypeError("Y muss eine Zahl sein")
    
    def distance(self, other):
        if isinstance(other, Point):
            return math.sqrt(((x - other.x) ** 2) + ((y - other.y) ** 2))
        else:
            raise TypeError("Distanzen können nur zwischen Punkten berechnet werden")
    
    def area(self):
        return 0
    
    def __eq__(self, other):
        if (self.x == other.x and self.y == other.y):
            return True
        return False
    
    def __lt__(self, other):
        if (self.x < other.x):
            return True
        elif (self.x == other.x and self.y < other.y):
            return True
        return False
        
    def __gt__(self, other):
        if (self.x > other.x):
            return True
        elif (self.x == other.x and self.y > other.y):
            return True
        return False


#Liste mit allen Listen der größe n
list_of_lists = []
for i in n:
    new_list = sorted(
    [(Point(randint(-5000,5000), randint(-5000,5000)).x, Point(randint(-5000,5000), randint(-5000,5000)).y) for j in range(i)])
    list_of_lists.append(new_list)


#1x Suchen
linearesuche(list_of_lists[-1], random.choice(list_of_lists[-1]))
binaeresuche(list_of_lists[-1], random.choice(list_of_lists[-1]))

#10x Suchen
for i in range(10):
    start_time = time.monotonic()
    linearesuche(list_of_lists[-1], random.choice(list_of_lists[-1]))
    end_time = time.monotonic()
    print(timedelta(milliseconds=end_time - start_time))

for i in range(10):
    start_time = time.monotonic()
    binaeresuche(list_of_lists[-1], random.choice(list_of_lists[-1]))
    end_time = time.monotonic()
    print(timedelta(milliseconds=end_time - start_time))


#Suchen in alle listen 5x
zeit_liear = []
for j in list_of_lists:
    for i in range(10):
        begin = time.time()
        linearesuche(j, random.choice(j))
        end = time.time()
        zeit_liear.append(end - begin)

zeit_b = []
for j in list_of_lists:
    for i in range(10):
        begin = time.time()
        binaeresuche(j, random.choice(j))
        end = time.time()
        zeit_b.append(end - begin)

ticks = []
for i in range(len(n)):
    ticks.append('listen_'+ str(i))

plt.figure (figsize=(20,10))
plt.bar([i for i in range(len(zeit_b))],zeit_b, color='b', linewidth=3, label='binaer')
plt.bar([i for i in range(len(zeit_liear))],zeit_liear, color='red', label='Linear')
plt.grid(color='#95a5a6', linestyle='--', linewidth=2, axis='y', alpha=0.7)
plt.xticks(range(0,140,10), ticks[:14],rotation=45)
#plt.xlabel('Zeit',fontsize=18)
plt.ylabel('Dauer in s',fontsize=18)
plt.legend(fontsize=18)
plt.show()