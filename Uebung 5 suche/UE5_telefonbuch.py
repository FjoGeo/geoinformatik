#UE5 DataMod SS2020 | help: gueren.dinga@hcu-hamburg.de
# Vervollständigt die Funktionen "linearsearch" und "binaeresuche", sodass
# die Position der gesuchten Person ausgegeben wird. Wenn ihr nett seid,
# printed ihr so etwas wie "NAME befindet sich an Stelle X unserer Liste"


########## LINEARE SUCHE ##########
liste = ["ARIEL", "ELLA", "INGA", "KATHARINA", "LENA", "LINUS", "MARKUS", "MARTIN", "SOPHIE", "TIM"] #unser "Telefonbuch"
suche = "TIM" # Der Namen den wir suchen
n = [5, 10, 25, 50, 75, 100, 250, 500, 1000, 2500, 5000, 10000, 50000, 100000]

def linearesuche(liste,suche):
    for n,i in enumerate(liste):
        if i == suche:
            print(f"\nGesuchter Namer befindet sich an Position {n}")
            return n
    return -1


linearesuche = linearesuche(liste,suche)

if linearesuche == -1:
    print(f"Name {suche} ist nicht im Telefonbuch")


########## BINAERE SUCHE ##########
def binaeresuche(liste, suche):
    links = 0
    rechts = len(liste) - 1

    while links <= rechts:
        mitte = (rechts + links) // 2

        if liste[mitte] < suche:
            links = mitte + 1

        elif liste[mitte] > suche:
            rechts = mitte - 1

        else:
            return mitte
    return -1


binaeresuche = binaeresuche(liste,suche)

if binaeresuche != -1:
    print(f"\nGesuchter Namer befindet sich an Position {str(binaeresuche)}")
else:
    print(f"Name {suche} ist nicht im Telefonbuch")
