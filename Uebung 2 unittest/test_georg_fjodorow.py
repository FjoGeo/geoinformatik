import unittest
from georg_fjodorow_unit import plus, minus, division, multiplication, function_1, degree_to_radians, radians_to_degree


class Testing(unittest.TestCase):
    def test_plus(self):
        self.assertEqual(plus(3, 2), 5)
        self.assertEqual(plus(1, -1), 0)
        self.assertEqual(plus(-25, 5), -20)
        self.assertEqual(plus(-1, -5), -6)

    def test_minus(self):
        self.assertEqual(minus(3, 2), 1)
        self.assertEqual(minus(1, -1), 2)
        self.assertEqual(minus(-25, 5), -30)
        self.assertEqual(minus(-1, -5), 4)

    def test_multi(self):
        self.assertEqual(multiplication(3, 3), 9)
        self.assertEqual(multiplication(-5, 3), -15)
        self.assertEqual(multiplication(-9, -9), 81)
        self.assertEqual(multiplication(0, 123), 0)

    def test_division(self):
        self.assertEqual(division(3, 3), 1)
        self.assertEqual(division(-81, 9), -9)
        self.assertEqual(division(0, 3), 0)

    def test_function_1(self):
        self.assertEqual(function_1(3, 4), 35)
        self.assertEqual(round(function_1(1, 2), 2), 6.24)

    def test_degree_to_rad(self):
        self.assertEqual(round(degree_to_radians(180), 2), 3.14)
        self.assertEqual(round(degree_to_radians(90), 2), 1.57)

    def test_rad_to_degree(self):
        self.assertEqual(round(radians_to_degree(2), 2), 114.59)
        self.assertEqual(round(radians_to_degree(4), 2), 229.18)


if __name__ == "__main__":
    unittest.main()

