import math


def function_1(x, y):
    return (3*x**2) + math.sqrt((x**2) + (y**2)) + math.exp(math.log(x))


def radians_to_degree(x):
    return 180/math.pi * x


def degree_to_radians(x):
    return x * math.pi/180


def plus(x, y):
    return x+y


def minus(x, y):
    return x-y


def division(x, y):
    return x/y


def multiplication(x, y):
    return x*y